x=10
if x>10:
	print ("greater than 10")
	if x>20:
		print ("greater than 20")
else:
	print ("not greater than 10")

i=0
while i<100:
	print (i)
	i+=1

l=[1,2,3,4,5,6,7,[1,2,[],3], "what is going on?!?"]
print (l)
print (len(l))

m=[1,2,3,4,5,6,7,8]
m[3]=10
print (m)
print (m[2:4])
print (l+m)
print (3*m)
l.append(100)
print (l.count(100))
print (l.index(100))
l[l.index(100)]=99

l=[[1,2],[3,4]]
print(l[1][0])

print(list(range(3,10,2)))

p=[2,3,5,3,32,2]
for x in p:
	print (x)

i = 0
while (i<len(p)):
	print(p[i])
	i+=2

for i in range (0,len(p),2):
	print (p[i])

#l.remove(i)
print(p)
print(l.pop())
print(p.pop())
print (p)

print (dir(p))

from math import sqrt
print (sqrt(20))

import math
print(math.sqrt(20))

from random import *
seed()	#you seed once
print (randint(10,20))	#use random many times
print(random())
print (uniform(12,15))

for i in range (10):
	print(random())

rand()	#bad random generator, x=ax+b%c