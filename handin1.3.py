'''
Hand in 1, REA1121
16HBPROGA
Nataniel Gaasoy, 131390
'''

import numpy as np
import random

                             #----------------- Problem 1 a)-----------------------

def main():
    newLine= ("====================================================================")
    idVector= np.array([ 0.0,  0.0,  0.0,  0.0,  1.0])                                        #identity vector
              
    matrix= np.array([[ 120/1296,  900/1296,  250/1296,  25/1296,  1/1296],      #yahtzee probability transition matrix
           [ 0.0, 120/216,  80/216,  15/216,  1/216,],
           [ 0.0,  0.0,  25/36,  10/36,  1/36],
           [ 0.0,  0.0,  0.0,  5/6,  1/6],
           [ 0.0,  0.0,  0.0,  0.0,  1.0]])

    print ("The yahtzee probability transition matrix is: ")
    print (matrix)

    vector1= matrix.dot(idVector)
    #print(newLine)
    #print (vector1)            print the vector, for trouble shooting purposes

    vector2= matrix.dot(vector1)
    #print(newLine)
    #print (vector2)            print the vector, for trouble shooting purposes

    vector3= matrix.dot(vector2)
    #print(newLine)
    #print (vector3)            print the vector, for trouble shooting purposes

    print(newLine)
    print ("\nThe probability of yatzhee after 3 throws is: ", vector3[0])            #0.04602864
    print ("This is ", vector3[0]*100, "%")                                             #4.60286425257 %

                             #----------------- Problem 1 b)-----------------------
    vector4= matrix.dot(vector3)
    #print(newLine)
    #print (vector4)            print the vector, for trouble shooting purposes

    print(newLine)
    print ("\nThe probability of yatzhee after 4 throws is: ", vector4[0])            #0.100575361033
    print ("This is ", vector4[0]*100, "%")                                             #10.0575361033 %
    
                             #----------------- Problem 1 c)-----------------------

    v=1
    n=1

    latestVector= matrix.dot(idVector)

    while v<50:                                #runs untill probability of yahtzee is over 50%
        latestVector=matrix.dot(latestVector)   #adds the newest vector to the matrix multiplication
        v=latestVector[0]*100
        n+=1
    print(newLine)
    print ("The probability of yahtzee is over 50% after ", n, " throws. ")         #10 throws.

                            #----------------- Problem 1 c) part 2 -----------------------
    v=1
    n=1

    latestVector= matrix.dot(idVector)

    while v<90:                                #runs untill probability of yahtzee is over 90%
        latestVector=matrix.dot(latestVector)   #adds the newest vector to the matrix multiplication
        v=latestVector[0]*100
        n+=1
    print(newLine)
    print ("The probability of yahtzee is over 90% after ", n, " throws. ")       #19 throws.

                                 #----------------- Problem 1 d)-----------------------

    i=0
    n=1000000
    yahtzee=0

    while i<n:
        rand = random.uniform(0,1)                  #chooses a random percentage perimiter
        v=0
        latestVector= matrix.dot(idVector)
        while rand > v:                             #continues untill the probability of yahtzee is greater than "rand"
            latestVector=matrix.dot(latestVector)   #adds the newest vector to the matrix multiplication
            v=latestVector[0]
            i+=1
        yahtzee+=1                                  #adds 1 to total number of "yahtzee"
    print(newLine)
    print ("On average, you need ", n/(yahtzee), " throws to get a yahtzee. ")     #ca. 10, total throws divided by number of yahtzees

if __name__ == "__main__":
    main()