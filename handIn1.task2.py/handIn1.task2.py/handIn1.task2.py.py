'''
Hand in 1, REA1121. Task 2
16HBPROGA
Nataniel Gaasoy, 131390
'''

import numpy as np
import random

                             #----------------- Problem 2 a)-----------------------
def emptySnakes():      
    n=0

    gameMatrix = np.zeros((38, 38))  
    #assigning a matrix for the 6 X 6 grid, including a space for the start and end (outside of the board)

    while n<38:
        i=1
        while i<=6:
            if n+i>=38:
                gameMatrix [n][37]+=1/6   #adds 1/6 chance more for 5 steps from end, 2/6 more for 4 steps from end and so on
            else:
                gameMatrix [n][n+i]+=1/6   #sets the chance to get 1-6 steps from the current location to 1/6, [fra(|)][til(-)]
            i+=1
        n+=1
    return gameMatrix

def printr(matrix):     #Function to dispaly full matrix. Written by Jonas S. Solsvik, 16HBPROGA
    """return -- string representation of object"""  
    string = "\n---- " + str(38) + " X " + str(38) + " ----\n"

    for i in range(38):
        for j in range(38):

            if matrix[i][j] > 0:
                temp  = "{0:.3f}".format(matrix[i][j])          #number of decimals displayed
                string += "{0: <5}".format(temp)
            else:
                temp  = "{0:.0f}".format(matrix[i][j])
                string += "{0: <5}".format(temp)
        string += "\n"

    print (string)

def createShortcut(matrix, fromState, toState):
    i=1

    while i<=6:
       matrix[fromState-i][toState]+=matrix[fromState-i][fromState]  #adds the value of "fromState" to "toState"
       matrix[fromState-i][fromState]=0  
            #sets the value of "fromState" to 0, as there is no probability of staying here
       i+=1


def main():
    newLine= ("====================================================================")

                                 #----------------- Problem 2 b)-----------------------

    #printr (emptySnakes())           #  check for printing the game matrix before "shortcuts"

    count=1         #sets count to 1 as the first throw (newMatrix) starts outside the loop
    matrix = emptySnakes()      #need to define the matrix in order to save current "snakes and ladders" board

    createShortcut(matrix,  7, 21)       #ladder from space 8 to 22
    createShortcut(matrix,  8, 14)       #ladder from space 9 to 15
    createShortcut(matrix, 11, 30)       #ladder from space 12 to 31
    createShortcut(matrix, 30, 12)       #snake from space 31 to 13
    createShortcut(matrix, 24, 13)       #snake from space 25 to 14
    createShortcut(matrix, 10,  6)       #snake from space 11 to 7

    #printr (matrix)         #check for how the matrix looks with shortcuts
    
    newMatrix=matrix.dot(matrix)

    while newMatrix[0][37]==0:
        count +=1
        newMatrix=np.dot(newMatrix, matrix)     
    print ("Least number of throws needed to finish the game is: ", count)     
             #3 throws
    print (newLine)

                                     #----------------- Problem 2 c)-----------------------

    throw=1         #sets count to 1 as the first throw (newerMatrix) starts outside the loop
    
    newerMatrix=matrix.dot(matrix)     #squares the matrix to chechk probability after 2 throws
    
    while newerMatrix[0][37]<=0.5:       #loop to check when the probability of a finished game is over 50%
        throw +=1
        newerMatrix=np.dot(newerMatrix, matrix)      #continues to check the probabilities after each throw
    print ("Game finish probability over 50% at: ", throw, " throws. ")   
                #9 throws
    print (newLine)

    while newerMatrix[0][37]<=0.99:       #loop to check when the probability of a finished game is over 99%
        throw +=1
        newerMatrix=np.dot(newerMatrix, matrix)      #continues to check the probabilities after each throw
    print ("Game finish probability over 99% at: ", throw, " throws. ")   
                #31 throws
    print (newLine)

if __name__ == "__main__":
    main()