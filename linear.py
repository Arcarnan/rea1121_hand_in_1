#!/usr/local/bin/python3
#-*-coding:utf8-*-

"""
File      :  linear.py
Date      :  26.01.2017
Module    :  Innlevering 1 - REA1121 Matematikk for programmering
Authors   :  Jonas J. Solsvik - jonasjso@stud.ntnu.no
             Aksel Hjerpbakk
"""

import numpy as np                             # array, zeros

class Matrix:
    """ 
    class -- A class that represents the [n x m] - matrix
    """
    def __init__(self, nparray):               # nparray should be of type np.array()

        self.array  = nparray
        self.height = len(self.array)          # n = rows
        self.width  = len(self.array[0])       # m = columns

    def __mul__(self, other, debug=False):
        """operator -- Overloading * (multiplication) """
        count = 0
        if self.width == other.height:         # If matrix dimensions are compatible
            result_matrix = np.zeros((self.height, other.width)) 

            for i in range(other.width):
                for j in range(self.height):
                    for k in range(self.width):
                        result_matrix[j][i] += self.array[j][k] * other.array[k][i]

                        if debug:                                              
                            count += 1
                            print("#", count, ":  i:", i, " j:",  j, " k:", k)
            
            return Matrix(result_matrix)
        else:
            print("Wrong dimensions!")
            return self

    def __str__(self):
        """return -- string representation of object"""
        return "Matrix with " + str(self.height)  + " rows and " + str(self.width) + " columns"

    def __repr__(self):
        """return -- string representation of object"""  
        string = "\n---- " + str(self.height) + " X " + str(self.width) + " ----\n"

        for i in range(self.height):
            for j in range(self.width):
                string += "{0: >6}".format(str(self.array[i][j]))
            string += "\n"

        return string


def test_multiplication():
                                # Declaring 3 matrices
    matrix1 = Matrix(np.array([[1,2,4],[3,4,3]]))
    matrix2 = Matrix(np.array([[1,2], [2,3],[3,4]]))
    matrix3 = None
    
    print(" Initial matrices")
    print("=================")
    print(matrix1)
    print(repr(matrix1))
    print(matrix2)
    print(repr(matrix2))
                                
    matrix3 = matrix1 * matrix2         # Multiplying and getting a new matrix

    print("Result matrix")
    print("=============")
    print(matrix3)
    print(repr(matrix3))


if __name__ == "__main__":
    test_multiplication()



