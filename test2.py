from random import *

#chance of getting two 6-es on two dice rolls

count=0
i=0
n=100000
while i<n:
	i+=1
	if randint (1,6)==6 and randint (1,6)==6:
		count+=1

print (count/n)

#chance of getting six 6-es on six dice rolls

count=0
i=0
n=100000
while i<n:
	i+=1
	allsix=True
	for j in range (6):
		if randint(1,6)!=6:
			allsix=False
		if allsix: count +=1


print (count/n)
print (1/36)

#calculate area of unit circle

count=0.0
n=100000
i=0
while i<n:
	i+=1
	x=uniform(-1,1)
	y=uniform(-1,1)
	if x*x+y*y<i:	#test for inside circle
		count +=1

print(4*count/n)


#problem:
#	bus with 20 people	
#	10 bus stops
#	people get off at uniformly distributed random stops
#	question: how many times does the bus stop?

count = 0.0
n=1000
i=0
while i<n:
	i+=1
	l= [randint(1,10) for j in range (20)]
	m=set(1)
	count +=len(m)


print (count/n)

'''										l=[]
										for i in range (2=):
										l.append(randint(1,10))		alternative method

									m=[]
										for x in l:
											if x not in m:
												m.append(x)
										count +=len(m)				alternative method'''


maze=[[1,2],[0,3],[0],[1]]
n=1000
i=0
count=0.0
while i<n:
	i+=1
	state=0
	while state !=3:
		count +=1
		#print(state)	trouble shoot 
		state=maze[state][randint(0, len(maze[state])-1)]

print(count/n)
